/*
 * Copyright © 2019 Hedzr Yeh.
 */

package cmdraddons

const (
	// AppName const
	AppName = "cmdr-addons"
	// Version const
	Version = "1.10.47"
	// VersionInt const
	VersionInt = 0x010a2f
)
